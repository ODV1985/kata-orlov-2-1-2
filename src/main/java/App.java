import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;

public class App {
    public static void main(String[] args) {
        ApplicationContext context =
                new AnnotationConfigApplicationContext(AppConfig.class);

        HelloWorld beanHW1 = context.getBean("helloWorldBeanId", HelloWorld.class);
        System.out.println(beanHW1.getMessage());

        HelloWorld beanHW2 = context.getBean("helloWorldBeanId", HelloWorld.class);
        System.out.println(beanHW2.getMessage());


        Cat cat1 = context.getBean("catBeanId", Cat.class);
        System.out.println(cat1.getMessage());

        Cat cat2 = context.getBean("catBeanId", Cat.class);
        System.out.println(cat1.getMessage());

        System.out.println("Bean Hello World это один и тот же Bean? = " + (beanHW1 == beanHW2));
        System.out.println("Bean Cat это один и тот же Bean? = " + (cat1 == cat2));
    }
}